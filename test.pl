% must allow changing predicates in runtime

% :- use_module(library(chr)).
% :- chr_constraint action/2.


% works
% precond2(a).

% effect2(X) :- precond2(X).
% effect2(X) :- not(precond2(X)).


% also works
% precond(a).
% not_precond(a).
% not_precond(X) :- not(precond(X)).

% effect(X) :- precond(X).
% effect(X) :- not_precond(X).


% precond(Now) :- action(Now, Then), effect(Then). %, effect2(Then).


% forward and backward? use multiple versions of same predicate to eliminate infinite cycles?
action(now, then).
action(Now, Then) :- precond(Now), effect_f(Then), effect2_f(Then).
action(Now, Then) :- not(precond(Now)), effect_f(Then), not(effect2_f(Then)).

effect(Then) :- action(Now, Then), precond(Now).
effect(Then) :- action(Now, Then), not(precond(Now)).

effect2(Then) :- action(Now, Then), precond(Now).

effect_f(then).
effect2_f(then).

check_effect(Then) :- effect(Then), effect_f(Then).
check_effect2(Then) :- effect2(Then), effect2_f(Then).


% action(now, then).
% action(Now, Then) :- precond(Now), !, effect(Now, Then). % effect2(Then).
% action(Now, Then) :- not(precond(Now)), !, effect(Now, Then). %, not(effect2(Then)).

% effect(Now, Then) :- action(Now, Then), precond(Now).
% effect(Now, Then) :- action(Now, Then), not(precond(Now)).





% effect2(then).
% effect2(Then) :- precond(Now), action(Now, Then).



% precond(now).
% precond(Now) :- action(Now, Then), effect(Then).

% action(now, then).
% action(Now, Then) :- precond(Now), effect(Then).
% action(Now, Then) :- not(precond(Now)), not(effect(Then)).

% effect(then).
% effect(Then) :- (precond(Now), action(Now, Then)).



% effect2(then).
% effect2(Then) :- precond(Now), action(Now, Then).


% precond(Now) :- action(Now, Then), effect(Then), effect2(Then).

% action(now, then).
% action(Now, Then) :- precond(Now), effect(Then), effect2(Then).
% action(Now, Then) :- not(precond(Now)), effect(Then), not(effect2(Then)).

% effect(Then) :- (precond(Now), action(Now, Then)); ( not(precond(Now)), action(Now, Then) ).

% effect2(then).
% effect2(Then) :- precond(Now), action(Now, Then).
